const router = require('express').Router();
const mongoose=require("mongoose");
const fonctions=require("../fonctions");

require("../models/Recette");
require("../models/User");
require("../models/Images");
const Recette=mongoose.model("Recette");
const User=mongoose.model("User");
const Images=mongoose.model("Images");

const auth = require('./auth');

var logged=false;
var userLogged={nom:"",recettesTestees:[]};

clean=fonctions.clean; 

genereLien=fonctions.genereLien;

router.get("/isLogged",auth.optional,function(req,res){
    if(logged){
        User.findOne({account:userLogged.nom},function(err,user){
            userLogged.recettesTestees=user.recettesTestees;
            res.send({logged:logged,user:userLogged,token:user.generateJWT()});
        });   
    }else{
        res.send({logged:logged,user:userLogged});
    }
});


router.get("/recettes",auth.required,function(req,res,next){
    Recette.find({},{__v:0},function(err,recettes){
        res.send(recettes);
    });

});

router.get("/recettes/:recetteUrl",auth.required,function(req,res){
    Recette.find({url:req.params.recetteUrl },{_id:0,__v:0},function(err,recette){
        res.send(recette[0]);
    });
});

router.post("/auth",auth.optional,function(req,res){
    User.findOne({account:req.body.account},function(err,user){
        if(user && user.validatePassword(req.body.password)){
            token=user.generateJWT();
            res.send({nom:user.account,logged:true,recettesTestees:user.recettesTestees,token:token});
        }else{
            logged=true;
            userLogged.nom=user.account;
            res.send({logged:false});
        }   
    });
});



router.post("/addUser",auth.optional,function(req,res){
    let nom=req.body.account;
    User.findOne({account:nom},function(err,user){
        if(err){
            res.send(err);
        }
        if(user==null){
            newUser = new User({account:nom,recettesTestees:[]});
            newUser.setPassword(req.body.password);
            newUser.save().then(() => res.json({ user: newUser.toAuthJSON() }));
        }else{
            res.send("existe deja");
        }
    });
});


router.post("/testeRecette/:userNom",auth.required,function(req,res){
    User.findOne({account:req.params.userNom},function(err,user){
        if(err){
            res.send(err);
        }else{
            if(user.recettesTestees.includes(req.body.recetteNom)){
                res.send(false);
            }else{
                user.recettesTestees.push(req.body.recetteNom);
                User.update({_id:user._id},{account:user.account,
                    password:user.password,
                    recettesTestees:user.recettesTestees},function(err){
                        if(!err){
                            res.send(true);
                        }
                    });
            }
        }
    });
});

router.post("/ajout",auth.required,function(req,res){
    let urlReq=genereLien(req.body.nom);
    Recette.findOne({url:urlReq},function(err,recette){
        if(err){
            res.send(err);
        }
        if(recette==null){
            Recette.create({nom:req.body.nom,
                    url:urlReq,
                    difficulte:req.body.difficulte,
                    origine:req.body.origine,
                    ingredients:clean(req.body.ingredients),
                    vegan:req.body.vegan,
                    description:req.body.description,
                    commentaires:[]},function(err,res2){
                if(err){
                    console.log(err);
                }else{
                    res.send("ok");
                }
            });
        }else{
            res.send("existe deja");
        }
    });
});



router.post("/modifie/:recetteUrl",auth.required,function(req,res){
    Recette.findOne({url:req.params.recetteUrl},function(err,recette){
        Recette.update({_id:recette._id},{nom:recette.nom,
                                        url:req.params.recetteUrl,
                                        difficulte:req.body.difficulte,
                                        origine:req.body.origine,
                                        ingredients:clean(req.body.ingredients),
                                        vegan:req.body.vegan,
                                        description:req.body.description,
                                        commentaires:recette.commentaires},function(err,res){
            if(err){
                console.log(err);
            }
        });
        res.send(recette);
    });

});

router.post("/commente/:recetteUrl",auth.required,function(req,res1){
    Recette.findOne({url:req.params.recetteUrl},function(err,recette){
        recette.commentaires.push(req.body.commentaire);
            Recette.update({_id:recette._id},{nom:recette.nom,
                url:recette.url,
                difficulte:recette.difficulte,
                origine:recette.origine,
                ingredients:recette.ingredients,
                vegan:recette.vegan,
                description:recette.description,
                commentaires:recette.commentaires},function(err,res2){
                    if(err){
                        console.log(err);
                    }else{
                        res1.send(recette.commentaires);
                    }
                }
            );
    });
});

router.get("/supprime/:recetteUrl",auth.required,function(req,res){
    Recette.deleteOne({url:req.params.recetteUrl},function(err){
        if(err){
            res.send(err);
        }else{
            res.send("ok");
        }
    });
});

const fs=require("fs"); 
const multer = require('multer');
const upload = multer({ dest: 'Images/' });


router.post("/addImage/:recetteUrl",auth.required,upload.single('image'),function(req,res1){
    let imgPath="Images/"+req.params.recetteUrl+".jpg";
    fs.rename("Images/"+req.file.filename,imgPath,function(err){
        if(err){
            console.log(err);
        }else{
            Images.create({url:req.params.recetteUrl,
                image: {data:fs.readFileSync(imgPath),
                        contentType:"img/jpeg"}},function(err,res2){
                            if(err){
                                console.log(err);
                            }else{
                                fs.unlink(imgPath,function(err){});
                            }
                        });
            res1.send(true);
        }
    });
});

router.get("/images/:recetteUrl",auth.required,function(req,res){
    Images.findOne({url:req.params.recetteUrl},function(err,img){
        if(img==null){
            res.send(false);
        }else{
            var base64data = new Buffer(img.image.data, 'binary').toString('base64');
            res.send({image:base64data});
        }
    });
});

router.get("/ajoutAuto/:recetteUrl", auth.required, function(req,res){
    var PythonShell = require('python-shell');
    var pyshell = new PythonShell('./script/add_recipe_marmiton.py');
    pyshell.send(req.params.recetteUrl);
    pyshell.end(function (err,code,signal) {
        if (err) throw err;
        console.log('finished');
        res.send('finished');
      });
});



module.exports = {router:router};