import urllib
import requests
from bs4 import BeautifulSoup
import ssl
import re


recette=input("Rentrez une recette : ")

url = "http://www.marmiton.org/recettes/recherche.aspx?type=all&aqt="+recette



r = requests.get(url)
soup = BeautifulSoup(r.text, "html5lib")
links=soup.findAll("a")
for link in links:
    if link.has_attr("class") and link["class"]==["recipe-card"] and "video" not in link["href"]:
        recipe=link
        break
print(recipe["href"])
r2 = requests.get(recipe["href"])
soup2 = BeautifulSoup(r2.text, "html5lib")
links2=soup2.findAll("ul")
for link in links2:
    if link.has_attr("class") and link["class"]==['recipe-ingredients__list']:
        ingredients=link
        break

recette={}

regex = re.compile(r"(cl +de +)|(cl +d' *)|(g +de +)|(g +d' *)|(kg +de +)|(L +de +)|(ml +de +)|(mg +de +)|(kg +d' *)|(L +d' *)|(ml +d' *)|(mg +d' *)")
ingredientsListe=[]
for ingredient in ingredients.findAll("li"):
    if ingredient.span.contents!=[]:
        ingredientsListe.append({"nom": regex.sub("",ingredient.p["data-name-singular"]), "quantite": eval(ingredient.span.contents[0])})
    else:
        ingredientsListe.append({"nom": ingredient.p["data-name-singular"], "quantite": 50})
recette["ingredients"]=ingredientsListe

links2=soup2.findAll("h1")
s=links2[0].contents[0]
s=s[0].upper()+s[1:].lower()
recette["nom"]=s

links2=soup2.findAll("div")
for link in links2:
    if link.has_attr("class") and link["class"]==['recipe-infos']:
        infos=link
        break
def traduitDifficulte(diff):
    if diff=="très facile":
        return 1
    if diff=="facile":
        return 2
        
for info in infos.findAll("div"):
    if len(info.findAll("span"))==1 and info["class"][0]=='recipe-infos__level':
        recette["difficulte"]=traduitDifficulte(info.findAll("span")[0].contents[0])
        
links2=soup2.findAll("ol")
for link in links2:
    if link.has_attr("class") and link["class"]==['recipe-preparation__list']:
        preparation=link
        break
        
regex = re.compile(r'[\n\r\t]')
s=""
for etape in preparation.findAll("li"):
    for content in etape.contents:
        if "" in content:
            s+=content
        elif content.has_attr("href"):
            s+=content.contents[0]
    s+=" "
s=regex.sub("",s)
recette["description"]=s

recette["origine"]="France"

viandes=["boeuf","poulet","agneau","veau","saucisse","côte","steak","lapin","canard","volaille","oeufs","omelette","saumon","thon","crevette"]
recette["vegan"]=True
for viande in viandes:
    if viande in recette["nom"].lower():
        recette["vegan"]=False
        break

token="Token eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50IjoiUGllcnJlIiwicmVjZXR0ZXNUZXN0ZWVzIjpbXSwiZXhwIjoxNTM2NTkxNzQxLCJpYXQiOjE1MzE0MDc3NDF9.w4xuHZBysj64PjJ1b2ZW01KrqlbfDzXwFxCY-lOYQhw"
requests.post("http://localhost:8080/ajout",json=recette,headers={"authorization":token})
print("Done")


    
    



        
