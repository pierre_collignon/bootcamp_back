const request = require("request");
const mongoose=require("mongoose");
require("../models/User");
const User= mongoose.model("User");

var base_url = "http://localhost:8080";
userTest=new User({account:"test",recettesTestees:[]});


describe("Router", function() {

    describe("auth", function() {
        it("returns status code 200", function(done) {
            request.post({url:base_url+"/auth", body:JSON.stringify({account:"test", password:"test"}), headers: {'Content-Type': 'application/json'}}, function(error, response, body) {
                expect(response.statusCode).toBe(200);
                done();
            });
        });
        it("has the right error", function(done){
            request.post({url:base_url+"/auth", body:JSON.stringify({account:"test", password:""}), headers: {'Content-Type': 'application/json'}}, function(error, response, body) {
                expect(JSON.parse(response.body).logged).toBe(false);
                done();
            });
        });
        it("has the right body", function(done) {
            request.post({url:base_url+"/auth", body:JSON.stringify({account:"test", password:"test"}), headers: {'Content-Type': 'application/json'}}, function(error, response, body) {
                expect(response.body).toContain("token");
                done();
            });
        });
    });

    describe("ajoutRecette", function() {
        it("returns status code 200", function(done) {
            token=userTest.generateJWT();
            request.post({url:base_url+"/ajout",
                        body:JSON.stringify({nom:"test",
                            difficulte:1,
                            origine:"test",
                            ingredients:[{nom:"test",quantite:0}],
                            vegan:true,
                            description:"test"}),
                        headers: {'Content-Type': 'application/json','authorization': "Token "+token}}, function(error, response, body) {
                expect(response.statusCode).toBe(200);
                done();
            });
        });
        it("doesn't allow duplicates", function(done) {
            token=userTest.generateJWT();
            request.post({url:base_url+"/ajout",
                        body:JSON.stringify({nom:"test",
                            difficulte:1,
                            origine:"test",
                            ingredients:[{nom:"test",quantite:0}],
                            vegan:true,
                            description:"test"}),
                        headers: {'Content-Type': 'application/json','authorization': "Token "+token}}, function(error, response, body) {
                expect(response.body).toBe("existe deja");
                done();
            });
        });
    });


    describe("modifieRecette", function() {
        it("has the right body", function(done) {
            token=userTest.generateJWT();
            request.post({url:base_url+"/modifie/test",
                        body:JSON.stringify({nom:"test",
                            difficulte:2,
                            origine:"test",
                            ingredients:[{nom:"test",quantite:0}],
                            vegan:true,
                            description:"test"}),
                        headers: {'Content-Type': 'application/json','authorization': "Token "+token}}, function(error, response, body) {
                expect(JSON.parse(response.body).difficulte).toBe(1);
                done();
            });
        });
    });

    describe("commenteRecette",function(){
        it("has the right body", function(done){
            token=userTest.generateJWT();
            request.post({url:base_url+"/commente/test",
                        body:JSON.stringify({commentaire:{From:"test",content:"test"}}),
                        headers: {'Content-Type': 'application/json','authorization': "Token "+token}}, function(error, response, body) {
                expect(JSON.parse(response.body)[0].From).toBe("test");
                done();
            });
        });
    });

    describe("getRecette", function() {
        it("has the right body", function(done){
            token=userTest.generateJWT();
            request.get({url:base_url+"/recettes/test", headers: {'authorization': "Token "+token}}, function(error, response, body) {
                expect(JSON.parse(response.body).difficulte).toBe(2);
                expect(JSON.parse(response.body).commentaires[0].content).toBe("test");
                done();
            });
        });
        it("has the right error", function(done){
            token=userTest.generateJWT();
            request.get({url:base_url+"/recettes/?!..", headers: {'authorization': "Token "+token}}, function(error, response, body) {
                expect(response.statusCode).toBe(200);
                done();
            });
        });
    });

    describe("getRecettes", function() {
        it("returns status code 200", function(done) {
            token=userTest.generateJWT();
            request.get({url:base_url+"/recettes", headers: {'authorization': "Token "+token}}, function(error, response, body) {
                expect(response.statusCode).toBe(200);
                done();
            });
        });
        it("has the right body", function(done) {
            token=userTest.generateJWT();
            request.get({url:base_url+"/recettes", headers: {'authorization': "Token "+token}}, function(error, response, body) {
                expect(JSON.parse(response.body).length).toBeGreaterThan(0);
                expect(response.body).toContain("test");
                done();
            });
        });
    });

    describe("supprimeRecette", function() {
        it("has the right body", function(done){
            token=userTest.generateJWT();
            request.get({url:base_url+"/supprime/test", headers: {'authorization': "Token "+token}}, function(error, response, body) {
                expect(response.body).toBe("ok");
                done();
            });
        });
        it("has the right error", function(done){
            token=userTest.generateJWT();
            request.get({url:base_url+"/supprime/", headers: {'authorization': "Token "+token}}, function(error, response, body) {
                expect(response.statusCode).toBe(404);
                done();
            });
        });
    });

    describe("token", function() {
        it("has the right error", function(done) {
            request.get({url:base_url+"/recettes"}, function(error, response, body) {
                expect(response.statusCode).toBe(401);
                done();
            });
        });
    });
});