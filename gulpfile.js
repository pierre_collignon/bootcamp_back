const gulp = require('gulp');
const concat = require("gulp-concat");
const uglify = require("gulp-uglify-es").default;

const paths = {
    src: 'src/**/*',
    srcJS: 'src/**/*.js',

    tmp: 'tmp',
    tmpJS: 'tmp/**/*.js',

    dist: "dist",
    distJS: "dist/**/*.js"
}

gulp.task('copy', function () {
    return gulp.src(paths.srcJS).pipe(gulp.dest(paths.tmp));
});

gulp.task('copy:dist', function () {
    return gulp.src(paths.srcJS)
      .pipe(concat('script.min.js'))
      .pipe(uglify());

      //.pipe(gulp.dest(paths.dist));
});

gulp.task('watch', function () {
    gulp.watch(paths.src, ['copy']);
});

gulp.task('build', ['copy:dist']);

gulp.task('default', ['watch']);