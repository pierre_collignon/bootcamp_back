const mongoose=require("mongoose");

var SchemaRecette=new mongoose.Schema({
    nom : {type:String, required:true},
    url : {type:String, required:true},
    difficulte: {type:Number, required:true},
    origine: {type:String, required:true},
    ingredients: {type:Array, required:true},
    vegan: {type:Boolean, required:true},
    description: {type:String, required:true},
    commentaires: {type:Array, required:true}
});

mongoose.model("Recette", SchemaRecette);